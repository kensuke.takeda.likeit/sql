CREATE DATABASE benkyou1 DEFAULT CHARACTER SET utf8;
 USE benkyou1;

 CREATE TABLE item(
 item_id int  NOT NULL PRIMARY KEY AUTO_INCREMENT,
 item_name VARCHAR(256) NOT NULL,
 item_price int NOT NULL,
 category_id int
  );
